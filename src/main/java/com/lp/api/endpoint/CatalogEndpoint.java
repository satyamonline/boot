package com.lp.api.endpoint;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.broadleafcommerce.core.catalog.domain.Product;
import org.broadleafcommerce.core.catalog.service.CatalogService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lp.api.core.exception.BroadleafWebServicesException;
import com.lp.api.wrapper.CustomProductWrapper;
import com.lp.core.api.endpoint.BaseEndpoint;


/*@Component
@Scope("singleton")
@Path("/catalog/")
@Produces(value = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes(value = { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })*/
@RestController
public class CatalogEndpoint extends BaseEndpoint {

	 @Resource(name="blCatalogService")
	    protected CatalogService catalogService;
	 
	@RequestMapping("product/{id}")
	public CustomProductWrapper findProductById(@Context HttpServletRequest request,
			@PathParam("id") Long id) {
		Product product = catalogService.findProductById(id);
		if (product != null) {
			CustomProductWrapper wrapper;
			wrapper = (CustomProductWrapper) context
					.getBean(CustomProductWrapper.class.getName());
			wrapper.wrapDetails(product, request);
			return wrapper;
		}
		throw BroadleafWebServicesException.build(
				Response.Status.NOT_FOUND.getStatusCode()).addMessage(
				BroadleafWebServicesException.PRODUCT_NOT_FOUND, id);
	}
}
